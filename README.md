# Foundry HeXXen 1733 - Buch der Regeln

Dieses Projekt dient der Veröffentlichung und Verwaltung von Projektinformationen zum Kaufmodul. 

Wenn ihr das Modul kaufen wollt, kommt ihr hier zum [Shop](https://www.f-shop.de/detail/index/sArticle/2534).

Die Release-Notes könnt ihr [hier](https:/ulisses-spiele-official/foundry-hexxen-1733-bdr/-/releases) finden:  

Fehlermeldungen und Verbesserungsvorschläge können hier eingesehen bzw. eingereicht werden:
- [Übersicht](https:/ulisses-spiele-official/foundry-hexxen-1733-bdr/-/issues)
- [Fehlermeldung einreichen](https:/ulisses-spiele-official/foundry-hexxen-1733-bdr/-/issues/new?issuable_template=Fehlerbericht)
- [Verbesserungsvorschlag einreichen](https:/ulisses-spiele-official/foundry-hexxen-1733-bdr/-/issues/new?issuable_template=Verbesserungsvorschlag)
